<?php

function des($x = array()) {

    if (empty($x)) {
        $x[] = 130;
        $x[] = 145;
        $x[] = 150;
        $x[] = 165;
        $x[] = 170;
        $x[] = 170;
    }


    $n = count($x);
    $alpha = 2 / ($n + 1);

    foreach ($x as $t => $value) {
        if ($t == 0) {
            $sa[$t] = $value;
            $saa[$t] = $value;
            $a[$t] = 0;
            $b[$t] = 0;
            $f[$t] = 0;
        } else if ($t == 1) {
            $sa[$t] = ($alpha * $x[$t]) + ((1 - $alpha) * $sa[($t - 1)]);
            $saa[$t] = ($alpha * $sa[$t]) + ((1 - $alpha) * $saa[($t - 1)]);
            $a[$t] = (2 * $sa[$t]) - $saa[$t];
            $b[$t] = (($alpha) / (1 - $alpha)) * ($sa[$t] - $saa[$t]);
            $f[$t] = 0;
        } else {
            $sa[$t] = ($alpha * $x[$t]) + ((1 - $alpha) * $sa[($t - 1)]);
            $saa[$t] = ($alpha * $sa[$t]) + ((1 - $alpha) * $saa[($t - 1)]);
            $a[$t] = (2 * $sa[$t]) - $saa[$t];
            $b[$t] = (($alpha) / (1 - $alpha)) * ($sa[$t] - $saa[$t]);
            $f[$t] = $a[$t] + $b[$t];
        }
        
        
         if ($t <= 1) {
                $mad[$t] = 0;
                $mse[$t] = 0;
            } else {
                $mad[$t] = abs($value - $f[$t]);
                $mse[$t]=  pow(($value - $f[$t]),2);
            }
            
            $mape[$t] = ($mad[$t] / $value) * 100;
    }
    
     
            

    $data['t'] = $t;
    $data['sa'] = $sa;
    $data['saa'] = $saa;
    $data['a'] = $a;
    $data['b'] = $b;
    $data['f'] = $f;
    $data['mad'] = $mad;
    $data['mse'] = $mse;
    $data['mape'] = $mape;



    return $data;
}
